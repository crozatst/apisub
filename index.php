<?php
session_start();

include 'lib/cas.php';
$cas = new CAS('https://cas.utc.fr/cas/','https');
$cas->connect();

include 'lib/db.php';
include 'lib/views.php';
include 'lib/admin.php';

$db = new DB();

// Do a local copy once per session only ; $_SESSION['dolocalcopy'] is set to true by $cas->connect()
if ($_SESSION['dolocalcopy']) {
  $db->copyUser($_SESSION['utclogin'], $_SESSION['surname'], $_SESSION['firstname'], $_SESSION['mail'], $_SESSION['level']);
  $_SESSION['dolocalcopy'] = false;
}
$admin = new Admin($db);
$view = new Views($admin, $_SESSION['utclogin'], $_SESSION['surname'], $_SESSION['firstname']);

$view->printHtmlBegin($admin);
$view->printUser($db->isResp($_SESSION['utclogin']), $db->isAdmin($_SESSION['utclogin']));

if (isset($_GET['mode'])) {
  /** Logout */
  if ($_GET['mode']=='logout') { 
    $cas->logout();
  }
  /** Api Resp actions : Validate or Decline **/
  if ($_GET['mode']=='resp' && $db->isResp($_SESSION['utclogin']))  {
    if (isset($_GET['action']) && isset($_GET['api']) && isset($_GET['login'])) {
      if ($_GET['action']=='validate' && $admin->isValidatable()) {
        $db->validate($_SESSION['utclogin'], $_GET['api'], $_GET['login'], 'TRUE');
      }
      elseif ($_GET['action']=='decline') {
        $db->validate($_SESSION['utclogin'], $_GET['api'], $_GET['login'], 'FALSE');
      }
    }
    $view->respValidation($db->apiListResp($admin, $_SESSION['utclogin']), $admin);
  }
  /** Admin actions : Unsubscribe a Student (unsubToApi) ; manage subscription dates and activation **/
  if ($_GET['mode']=='admin' && $db->isAdmin($_SESSION['utclogin'])) {
    if (isset($_GET['action'])) {
      if ($_GET['action']=='cancel') {
          $db->unsubToApi($_GET['login'], $_GET['api'], $_SESSION['utclogin']);
        }   
      if ($_GET['action']=='activate') {        
        $db->activateSubscription();
      }
      if ($_GET['action']=='unactivate') {
        $db->unactivateSubscription();
      }
      if ($_GET['action']=='edit') {
        if (isset($_GET['begin'])) $db->updateOpenbegin($_GET['begin']);
        if (isset($_GET['end'])) $db->updateOpenend($_GET['end']);
        if (isset($_GET['days'])) $db->updateValidationdays($_GET['days']);
      }     
      if ($_GET['action']=='apisize') {
        $db->changeApiSize($_GET['api'], $_GET['size'], $_SESSION['utclogin']);
      }
    } 
    $view->adminFunction($_SESSION['utclogin'], $db->mailresp(), $db->apiListAll(), $db->apiStudentsAll(), $db->apiDeclined(), $db->stats(), $db->substats(), $admin);
  }
}
else {
  /** Student actions : Subscribe or Unsubscribe  **/
  if (isset($_GET['api'])) {
    if (isset($_GET['action'])) {
      if ($_GET['action']=='sub' && $admin->isActive()) {
        $db->subToApi($admin, $_SESSION['utclogin'], $_GET['api']);
      }
      elseif ($_GET['action']=='unsub') {
        $db->unsubToApi($_SESSION['utclogin'], $_GET['api']);
      }
    }
  }
  $view->printInstructions($admin);
  $view->printSubList($db->subList($_SESSION['utclogin']));
  $view->printApiList($db->apiList($admin, $_SESSION['utclogin']));
}
$view->printHtmlEnd();
?>
