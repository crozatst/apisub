BEGIN;

DROP TABLE IF EXISTS admins;
DROP TABLE IF EXISTS config CASCADE;

CREATE TABLE admins (
  utclogin TEXT PRIMARY KEY
);

INSERT INTO admins VALUES ('crozatst');
INSERT INTO admins VALUES ('dore');

CREATE TABLE config (
  closed BOOLEAN NOT NULL,
  openbegin DATE NOT NULL,
  openend DATE NOT NULL,
  validationdays INTEGER NOT NULL, -- number of days validation stays open after subscription is closed
  CHECK (openend > openbegin),
  CHECK (validationdays > 0)
);

-- Testing values
INSERT INTO config VALUES ('FALSE', TO_DATE('20231001','YYYYMMDD'), TO_DATE('20231231','YYYYMMDD'), 5);

CREATE OR REPLACE VIEW vconfig AS
SELECT
closed,
validationdays,
TO_CHAR(openbegin,'TMday FMDD TMmonth FMYYYY') AS openbegin,
TO_CHAR(openend,'TMday FMDD TMmonth FMYYYY') AS openend,
TO_CHAR(openend + validationdays,'TMday FMDD TMmonth FMYYYY') AS validationend,
semester(openend) AS semester,
year(openend) AS year,
NOT(closed) AND CURRENT_DATE >= openbegin AND CURRENT_DATE <= openend AS isactive,
NOT(closed) AND CURRENT_DATE >= openbegin AND CURRENT_DATE <= openend + validationdays AS isvalidatable
FROM config;

COMMIT;
