<?php

class Views {

    private $admin;
    private $utclogin;
    private $surname;
    private $firstname;

    public function __construct ($admin, $utclogin, $surname, $firstname) {
      $this->admin=$admin;
      $this->utclogin=$utclogin;
      $this->surname=$surname;
      $this->firstname=$firstname;
    }

    public function isActive() {
      return $this->admin->isActive();
    }

    public function isValidatable() {
      return $this->admin->isValidatable();
    }

    public function printHtmlBegin($admin) {
      echo '<html>';
      echo '<head>';
      echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>';
      echo '<title>Api Sub</title>';
      echo '<link href="css/main.css" type="text/css" rel="stylesheet"/>';
      echo '<script src="js/main.js" defer="true"></script>';
      echo '</head>';
      echo '<body>';
      echo '<h1>Foire aux Api '.$admin->activeSemester().$admin->activeYear().'</h1>';
    }

    public function printHtmlEnd() {
      echo '</body>';
      echo '</html>';      
    }

    public function printInstructions($admin) {
        echo '<hr/>';
        echo '<h2>Planning</h2>';
        echo '<ul>';
        if ($admin->isClosed()) {
          echo '<li><b><u>' . $admin->isClosedDisplay() . '</u></b></li>';
        }    
        echo '<li>Inscriptions aux Api du '.$admin->openbegin().' au <b>'.$admin->openend().'</b>.</li>';
        echo '<li>Validations des inscriptions du '.$admin->openbegin().' au <b>'.$admin->validationend().'</b>.</li>';
        echo '</ul>';
        echo '<h2>Catalogue</h2>';
        echo '<p>Les Api sont décrites ici : <b><a href="https://apint.utc.fr/cat">https://apint.utc.fr/cat</a></b>. Veillez à avoir bien lu la fiche avant de vous inscrire (pré-requis, dates...).</p>';
        echo '<h2>Instructions</h2>';
        echo '<ul>';
        echo '<li>Les Api sont ajoutées au fil du semestre par les responsables des Api. Dès qu\'une Api est au catalogue, les étudiants peuvent commencer à s\'inscrire.';
        echo '<li>Un étudiant peut s\'inscrire à plusieurs Api la même semaine, mais il ne pourra en suivre qu\'une seule. Il peut librement se désinscrire tant que l\'inscription n\'est pas validée.</li>';
        echo '<li><span class="goldenrule">Règle d\'or : dès qu\'un responsable d\'Api a validé une inscription, celle-ci est irréversible (ni l\'enseignant ni l\'étudiant ne peuvent l\'annuler). Si un étudiant était inscrit à d\'autres Api la même semaine, ces autres inscriptions sont annulées.</span></li>';
        echo '<li><span class="goldenrule">Corollaire : Si un responsable d\'Api commence à valider des inscriptions, alors l\'Api sera nécessairement ouverte.</span></li>';
        echo '</ul>';
    }

    public function printUser($isResp, $isAdmin) {
        echo "<div class='user'><span class='name'>$this->firstname $this->surname</span> (<span class='login'>$this->utclogin</span>)</div>";
        echo "<div class='menu'>";
        if ($isResp) {
          echo '<a href="index.php?mode=resp">[Gérer ses Api]</a> ';
        }
        if ($isAdmin) {
          echo '<a href="index.php?mode=admin">[Super-pouvoirs]</a> ';
        }
        if ($isResp || $isAdmin) {
          echo '<a href="index.php">[Inscriptions]</a> ';
        }
        echo '<a href="index.php?mode=logout">[Déconnexion]</a>';
        echo "</div>";
    }

    private function printApi($row) {
      return "<span>$row[normcode] - $row[name]</span> (du $row[normdbegin] au $row[normdend]) (Demandes $row[nbask] | Inscriptions $row[nbval]/$row[size])";
    }

    private function printWeek($row) {
      return "$row[year] Semaine $row[week]";
    }

    private function printStudent($row) {
      if (!$row['surname']) {
        return "$row[utclogin] $row[level]  <a href='mailto:$row[utclogin]@etu.utc.fr'>[mail]</a> <a href='https://demeter.utc.fr/portal/pls/portal30/portal30.get_photo_utilisateur?username=$row[utclogin]'>[photo]</a>";
      }
      else {
        return "$row[firstname] $row[surname] ($row[utclogin]) $row[level] <a href='mailto:$row[email]'>[mail]</a> <a href='https://demeter.utc.fr/portal/pls/portal30/portal30.get_photo_utilisateur?username=$row[utclogin]'>[photo]</a>";
      }
    }

    public function printSubList($list) {
      echo '<hr/>';
      if ($list) {
        echo '<h2>Vos inscriptions</h2>';
        $week = '';
        foreach ($list as $row) {
          if ($row['week'] != $week) {
            $week = $row['week'];
            echo "<h3>".$this->printWeek($row)."</h3>";
          }
          // If student has been validated
          if ($row['validation']) {
            echo '<p>';
            echo "<span class='validation'>[validé]</span> ";
            echo $this->printApi($row);
            echo '</p>';
          }
          // If student has not been validated and validation are still opened
          else if ($this->isValidatable()) {
            echo '<p>';
            // if subscriptions are still opened
            if ($this->isActive()) {
              $msg = 'désinscription';
            }
            // else, validation of subscriptions are still opened
            else {
              echo "<span class='wait'>[liste d'attente]</span> ";
              $msg = 'annulation';
            }
            echo "<a href='index.php?action=unsub&api=$row[id]' class='unsub'>[$msg]</a> ";
            echo $this->printApi($row);
            echo '</p>';
          }
          // If student has not been validated and validation are closed
          else {
            echo '<p>';
            echo "<span class='impossible'>[non validée] ";
            echo $this->printApi($row);
            echo "</span> ";
            echo '</p>';
          }
        }
      }
      else {
        echo '<h2>Aucune inscription</h2>';
      }
    }

  public function printApiList($list) {
    echo '<hr/>';
    if ($list) {
      echo '<h2>Liste des Api</h2>';
      $week = '';
      foreach ($list as $api) {
        if ($api['week'] != $week) {
          $week = $api['week'];
          echo "<h3>".$this->printWeek($api)."</h3>";
        }
        echo '<p>';
        if ($this->isActive()) {
          if ($api['validation'] == 1) {
            echo "[validée] ";
          }
          elseif ($api['validation'] == 0.5) {
            echo "[en attente] ";
          }
          elseif ($api['validation'] == -1) {
            echo "[<span class='impossible'>déclinée</span>] ";
          }
          elseif (! $api['is_available']) {
            echo "[<span class='impossible'>indisponible</span>] ";
          }
          elseif (! $api['open']) {
            echo "[<span class='impossible'>complet</span>] ";
          }
          else {
            echo "<span class='sub' id='$api[id]' name='$api[name]'>[inscription]</span> ";
          }
        }
        echo $this->printApi($api);
        echo '</p>';
      }
    }
    else {
      echo '<h2>Aucune Api n\'est ouverte aux inscriptions</h2>';
    }
  }

  public function respValidation ($listsub, $admin) {
    // #KNOWN_BUG Si un resp a plusieurs Api, il peut changer un inscrit entre ses deux Api en modifiant l'URL (pas via l'interface)

    echo '<h1>Interface Responsable d\'Api</h1>';
    echo '<hr/>';
    echo '<h2>Vadémécum</h2>';
    echo '<p><a href="https://apint.utc.fr/vademecum/">[Notice pour les responsables d\'Api]</a></p>';
    echo '<h2>Rappel</h2>';
    echo '<ul>';
    echo '<li>La validation d\'une inscription entraîne la confirmation de l\'ouverture de l\'Api.</li>';
    echo '<li>Toute inscription validée ou déclinée est irréversible.</li>';
    echo '<li>Il est possible de valider des demandes d\'inscription ' . $admin->validationDays() . ' jours après la fermeture des inscriptions.</li>';
    echo '</ul>';
    if ($listsub) {
      $api = '';
      foreach ($listsub as $sub) {
        if ($api != $sub['id']) {
          $api = $sub['id'];
          echo '<hr/>';
          echo "<h2>".$this->printApi($sub)."<a href='api.php?api=$api'> [listing]</a></h2>";
          echo "<h3></h3>";
        }
        if ($sub['utclogin']) {
          if ($sub['validation']) {
            // Subscriptions already validated
            echo '<p><b>[validé] </b>'.$this->printStudent($sub).'</p>';
          }
          else if (is_null($sub['validation']) && $this->isValidatable()) {
            // At least one waiting subscription
            echo "<p>
              <a href='index.php?action=validate&api=$sub[id]&login=$sub[utclogin]&mode=resp' class='valid'>[valider]</a> "
              .$this->printStudent($sub).
              " <span>(exclusivité $sub[indicator]%, inscription le $sub[subdate_text])</span>
              <a href='index.php?action=decline&api=$sub[id]&login=$sub[utclogin]&mode=resp' class='decline'>[décliner]</a>
              </p>";
          }
          else if (!$sub['validation']) {
            echo '<p>[décliné] <del>'.$this->printStudent($sub).'</del></p>';
          }
        }
        else if ($this->isValidatable()) {
          // Not yet any subscription for this Api (LEFT JOIN might have added null student line)
          echo "<p>Aucune inscription à gérer</p>";
        }
      }
    }
    else {
      echo "<p>Aucune Api à gérer</p>";
    }
  }

  public function apiStudents($students, $mode=true) {
    echo "<hr/>";
    if (isset($students[0])) {
      // Api Title
      if ($mode) {
        echo '<h2>'.$this->printApi($students[0]).'</h2>';
      }
      // Api mailing list
      $mailinglist='mailto:';
      foreach ($students as $s) {
        $mailinglist = $mailinglist."$s[email],";
      }
      // Api CSV data (for JavaScript function)
      if ($mode) {
        echo "<script>";
        echo "var csvstudents";
        echo " = [['Nom','Prénom','Niveau','Mail','Login'],";
        foreach ($students as $s) {
          echo "['$s[surname]','$s[firstname]','$s[level]','$s[email]','$s[utclogin]'],";
        }
        echo "];</script>";
      }
      // Api Menu
      // Validated students
      if ($mode) {
        echo "<div class='menu'> <b>Étudiants inscrits</b>
          <a href='$mailinglist'>[Mailing list]</a>
          <a id='csvlink' onclick='download_csv(csvstudents)' href=''>[Fichier CSV]</a>
          <span id='photos'>[Trombinoscope]</span>
        </div>";
        echo "<hr/>"; 
      }      
      // Waiting students
      else {
        echo "<div class='menu'> <b>Étudiants en attente</b>
          <a href='$mailinglist'>[Mailing list]</a>
        </div>";
        echo "<hr/>";       }
    }
    else {
      echo '<h2>Aucun inscrit validé</h2>';
    }
    foreach ($students as $s) {
      echo "<div class='student'>
        <img class='photo' src='https://demeter.utc.fr/portal/pls/portal30/portal30.get_photo_utilisateur?username=$s[utclogin]'/>
        <span class='name'>$s[surname] $s[firstname] | $s[level] | <a href='mailto:$s[email]'>$s[email]</a> ($s[utclogin])</span>
        </div>";
    }
  }

  public function apiStudentsCsv($students) {
    //TODO
    foreach ($students as $s) {
        echo "$s[surname];$s[firstname];$s[level];$s[email];$s[utclogin]\n";
    }
  }

  private function prepare ($value, $comma=true) {
     $v = '"'.addslashes($value).'"';
     if ($comma) $v = $v.',';
     return $v;
  }

  public function adminFunction ($utclogin, $mailresp, $apis, $students, $declined, $stats, $substats, $admin) {
    echo '<h2>Super-pouvoirs</h2>';
    // Resp Emails
    echo '<h3>Mails aux responsables d\'Api</h3>';
    echo '<div>';
    $all = '';
    foreach ($mailresp as $m) {
        echo "<a href='mailto:$m[email]'>$m[email]</a> |";
        if ($all) {
            $all = "$all,$m[email]";
        }
        else {
          $all = "$m[email]";
        }
    }
    echo "<b><a href='mailto:$all'>[all]</a></b>";
    echo '</div>';
    // Stats
    echo '<h3> Statistiques </h3>';
    $this->displayStats($stats);
    if ($substats) $this->displaySubscriptionStats($substats);
    // Date and activation management
    echo '<hr/>';
    echo '<h3> Administration </h3>';
    echo '<p><a id="edit" class="edit">[activer les modifications]</a></p>';
    echo '<h4> Administration des dates d\'inscriptions </h4>';
    echo '<div>';
    echo '<li>Début des inscriptions : <b>' . $admin->openBegin() . '</b></li>';
    echo '<form method="get" action="index.php" class="config">
            <input hidden="true" name="mode" value="admin"/>
            <input hidden="true" name="action" value="edit"/>
            <input type="date" name="begin"/>
            <input type="submit" value="Modifier"/>
          </form>';
    echo '<li>Fin des inscriptions : <b>' . $admin->openEnd() . '</b> (semestre ' . $admin->activeSemester() . $admin->activeYear() .') </li>';    
    echo '<form method="get" action="index.php" class="config">
            <input hidden="true" name="mode" value="admin"/>
            <input hidden="true" name="action" value="edit"/>
            <input type="date" name="end"/>
            <input type="submit" value="Modifier"/>
          </form>';
    echo '<li>Durée de validation après la fin des inscriptions : <b>' . $admin->validationDays() . ' jours</b> (fin de validation : ' . $admin->validationEnd() . ')</li>';
    echo '<form method="get" action="index.php" class="config">
            <input hidden="true" name="mode" value="admin"/>
            <input hidden="true" name="action" value="edit"/>
            <input type="number" name="days"/>
            <input type="submit" value="Modifier"/>
          </form>';
    echo '<li>' . $admin->isActiveDisplay() . ' | ' . $admin->isValidatableDisplay();
    if ($admin->isClosed()) {
      echo '<b> | <u>' . $admin->isClosedDisplay() . '</u></b> ';
      echo ' <a href="index.php?mode=admin&action=activate" class="edit config">[annuler la désactivation]</a>';
    }
    else {
      echo ' <a href="index.php?mode=admin&action=unactivate" class="edit config">[désactiver manuellement les inscriptions et validations]</a>';
    }
    echo '</li>';
    echo '</div>';

    // Students number admin
    echo '<h4> Administration des effectifs des Api</h4>';
    if ($apis) {
      foreach ($apis as $a) {
        echo '<div>';
        echo 'Semaine ' . $a["week"] . ' / Api ' . $a["normcode"] . ' - ' . $a["name"] . ' : <b>' . $a["size"] . '</b> places'; 
        echo '<form method="get" action="index.php" class="config">
          <input hidden="true" name="mode" value="admin"/>
          <input hidden="true" name="action" value="apisize"/>
          <input hidden="true" name="api" value="'
          .$a["id"]
          .'"/>
          <input type="number" size="4" name="size"/>
          <input type="submit" value="Modifier"/>
        </form>';        
      }
      echo '</div>';
    }

    // Student list
    echo '<hr/>';
    echo '<h3>Liste des inscriptions validées</h3>';
    if ($students) {
      echo '<script src="js/main.js"></script>';
      echo '<script>var csvstudents = [["Semaine","Code","Api","Nom","Prénom","Niveau","Mail","Login"],';
      foreach ($students as $s) {
        echo "["
          .$this->prepare($s["week"])
          .$this->prepare($s["normcode"])
          .$this->prepare($s["name"])
          .$this->prepare($s["firstname"])
          .$this->prepare($s["surname"])
          .$this->prepare($s["level"])
          .$this->prepare($s["email"])
          .$this->prepare($s["utclogin"], false)
          ."],";
      }
      echo "];</script>";
      // Api Menu
      echo "<p><a id='csvlink' onclick='download_csv(csvstudents)' href=''>[Fichier CSV]</a> ";
      echo "<span class='unsub' id='cancel'>[activer les désinscriptions (et les annulations de refus)]</span>";
      echo "</p>";
      foreach ($students as $s) {
        echo "<div>";
        echo "<span> Semaine $s[week] / Api $s[normcode] : $s[firstname] $s[surname] ($s[utclogin] · $s[level]) "
          ."<a href='index.php?mode=admin&action=cancel&api=$s[id]&login=$s[utclogin]' class='cancel'>[désinscrire]</a>"
          ."</span>";
        echo "</div>";
      }
    }
    echo '<h3>Liste des inscriptions déclinées</h3>';
    if ($declined) {
      foreach ($declined as $s) {
        echo "<div>";
        echo "<span> Semaine $s[week] / Api $s[normcode] : <del> $s[firstname] $s[surname] ($s[utclogin] · $s[level]) </del>"
        ."<a href='index.php?mode=admin&action=cancel&api=$s[id]&login=$s[utclogin]' class='cancel'>[annuler]</a>"
        ."</span>";
        echo "</div>";
      }
    }
  }

  private function displayStats($stats) {
    echo '<div>';   
    foreach ($stats as $s) {
      echo '<span> Places : ' . $s["size"] . '</span> |';
      echo '<span> Participants : ' . $s["total"] . '</span> |';
      echo '<span> Inscrits : ' . $s["subscription"] . '</span> |';
      echo '<span> En attente : ' . $s["waiting"] . '</span> |';      
      echo '<span> Déclinés : ' . $s["declined"] . '</span>';
    }
    echo '</div>';
  }

  private function displaySubscriptionStats($substats) {
    echo '<div>';   
    echo '<div class="histotitle"> Semaines d\'inscription </div>';
    foreach ($substats as $s) {
      echo '<div class="histodata">' . $s["week"] . ' ';
      for ($i=0; $i<$s["count"]; $i++) echo '·';
      echo '</div>'; 
    }
    echo '</div>';
  }

}
