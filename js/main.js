// Management of message when subscription is unavailable
document.querySelectorAll('.impossible').forEach( item => {
  item.addEventListener('click', event => {
      message = 'Vous ne pouvez pas vous inscrire à cette Api pour une des raisons suivantes : '
      message = addReason (message, 'le responsable de l\'Api a décliné votre demande')
      message = addReason (message, 'l\'Api est complète')
      message = addReason (message, 'vous avez une autre Api validée la même semaine')
      message = addReason (message, 'la date d\'ouverture des inscriptions aux Api est dépassée')
      message = addReason (message, 'la date de validation des inscriptions par les responsables d\'Api est dépassée')
      alert(message)
  })
})
function addReason (message, reason) {
  return message.concat('\n— ').concat(reason)
}

// Management of CSV downloading
function download_csv(students) {
    let csv = ''
    students.forEach(function(row) {
            csv += row.join(',')
            csv += '\n'
    });
    let a = document.getElementById('csvlink')
    a.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv)
    a.target = '_blank'
    a.download = 'people.csv'
}

// Management of subscription cancellation
try {
  document.querySelector('#cancel').addEventListener('click', activateCancel)
}
catch (e) {
  console.log('Subscription cancellation is not available (maybe not yet any subscription) : ' + e)
}

// Management of subscription dates 
try {
  document.querySelector('#edit').addEventListener('click', activateConfig)
}
catch (e) {
  console.log('Subscription dates management is not available, : ' + e)
}


function activateCancel () {
  document.querySelectorAll('.cancel').forEach( item => { item.style.display = 'inline' } )
  document.querySelectorAll('#cancel').forEach( item => { item.style.display = 'none' } )
}

function activateConfig () {  
  document.querySelectorAll('.config').forEach( item => { item.style.display = 'inline' } )
  document.querySelectorAll('#edit').forEach( item => { item.style.display = 'none' } )
}

// Management of trombinoscope
try {
  document.querySelector('#photos').addEventListener('click', displayPhotos)
}
catch (e) {
  console.log('Photos management is not available (maybe not yet any subscription) : ' + e)
}
function displayPhotos() {
  const height = '150px'
  const width = '120px'
  document.querySelectorAll('.photo').forEach( item =>
    {
      if (item.style.height === height) {
        item.style.height = '15px'
        item.style.width = '12px'
      }
      else {
        item.style.height = height
        item.style.width = width
      }
    }
  )
}

// Management of subscription confirmation  (display confirm message before calling PHP subscription page)
try {
  document.querySelectorAll('.sub').forEach( item => {
    item.addEventListener('click', event => {
      message = '1. Vous avez bien compris le fonctionnement de l\'inscription aux Api.'
        .concat('\n2. Vous souhaitez candidater pour l\'Api ').concat(item.nextElementSibling.textContent).concat('.')
       if (confirm(message)) {
         location.href = 'index.php?action=sub&api='.concat(item.id)
       }
     })
  })
}
catch (e) {
  console.log('To manage : ' + e)
}
