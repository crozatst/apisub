CREATE OR REPLACE FUNCTION unaccent_string(TEXT) RETURNS TEXT AS $$
  SELECT translate(
      $1,
      'âãäåÁÂÃÄÅèééêëÈÉÉÊËìíîïìÌÍÎÏÌóôõöÒÓÔÕÖùúûüÙÚÛÜ',
      'aaaaAAAAAeeeeeEEEEEiiiiiIIIIIooooOOOOOuuuuUUUU'
  );
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION nb_api(TEXT, TEXT) RETURNS INTEGER AS $$
/* Returns number of api for one week and year */
DECLARE
  w ALIAS FOR $1;
  y ALIAS FOR $2;
  c INTEGER;
BEGIN
  SELECT COUNT(*) INTO c
  FROM vapi
  WHERE week=w AND year=y;
  RETURN c;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION nb_sub(TEXT, TEXT, TEXT) RETURNS INTEGER AS $$
/* Returns number of subsciption for one student for one week and year */
DECLARE
  student ALIAS FOR $1;
  w ALIAS FOR $2;
  y ALIAS FOR $3;
  c INTEGER;
BEGIN
  SELECT COUNT(*) INTO c
  FROM vsubscription
  WHERE utclogin=student AND week=w AND year=y AND validation IS NULL;
  RETURN c;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION indicator(TEXT, TEXT, TEXT) RETURNS INTEGER AS $$
/* Returns indicator for one student for one week and year */
DECLARE
  student ALIAS FOR $1;
  w ALIAS FOR $2;
  y ALIAS FOR $3;
  nb_sub FLOAT;
  nb_api FLOAT;
BEGIN
  nb_sub = nb_sub(student,w,y);
  IF nb_sub = 0 THEN
    RETURN -1;
  ELSIF nb_sub = 1 THEN
    RETURN 100;
  ELSE
    RETURN ROUND(100 / nb_sub);
  END IF;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION is_available(TEXT, TEXT, TEXT) RETURNS BOOLEAN AS $$
/* Returns True if student has no api validated for same week and year */
DECLARE
  student ALIAS FOR $1;
  w ALIAS FOR $2;
  y ALIAS FOR $3;
  c INTEGER;
BEGIN
  SELECT COUNT(*) INTO c
  FROM vsubscription
  WHERE utclogin=student AND week=w AND year=y AND validation;
  IF c = 0 THEN
    RETURN 'TRUE';
  ELSE
    RETURN 'FALSE';
  END IF;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION semester(DATE) RETURNS CHAR(1) AS $$
/* Returns semestre from Api, using begin date */
DECLARE
  d ALIAS FOR $1;
  m INTEGER;
BEGIN
  m = DATE_PART('month', $1);
  /* Winter intersemester inscriptions begin in september and Api end in february */
  IF m >= 9 OR m <= 2 THEN
    RETURN 'H';
  /* Summer intersemester inscriptions begin in march and Api end in august */
  ELSIF m >= 3 OR m <= 8 THEN
    RETURN 'E';
  ELSE
    RETURN 'X';
  END IF;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION year(DATE) RETURNS CHAR(4) AS $$
/* Returns year from Api, using begin date */
DECLARE
  d ALIAS FOR $1;
  y INTEGER;
  m INTEGER;
BEGIN
  y = DATE_PART('year', $1);
  m = DATE_PART('month', $1);
  /* Winter intersemester inscriptions begin in september and Api end in february */
  IF m >= 9 AND m <=12 THEN
    RETURN y + 1;
  ELSIF m >=1 AND m <= 2 THEN
    RETURN y;
  /* Summer intersemester inscriptions begin in march and Api end in august */
  ELSIF m >= 3 OR m <= 8 THEN
    RETURN y;
  ELSE
    RETURN -1;
  END IF;
END;
$$ LANGUAGE PLPGSQL;
