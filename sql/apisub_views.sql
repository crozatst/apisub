BEGIN;

CREATE OR REPLACE VIEW vapi_subcount AS
SELECT
api AS subcountid,
COUNT(utclogin) AS countsub
FROM subscribe
WHERE validation IS NULL
GROUP BY api;

CREATE OR REPLACE VIEW vapi_valcount AS
SELECT
api AS valcountid,
COUNT(utclogin) AS countval
FROM subscribe
WHERE validation
GROUP BY api;

CREATE OR REPLACE VIEW vapi AS
SELECT
*,
TO_CHAR(dbegin,'TMday FMDD TMmonth') AS normdbegin,
LPAD(code::text, 4, '0') AS normcode,
TO_CHAR(dbegin + duration-1,'TMday FMDD TMmonth') AS normdend,
TO_CHAR(dbegin, 'IW') AS week,
TO_CHAR(dbegin, 'yyyy') AS year,
semester(dbegin) AS semester,
COALESCE(countsub,0) AS nbask,
COALESCE(countval,0) AS nbval,
CASE WHEN countval >= size THEN 0 ELSE 1 END AS open
FROM api LEFT JOIN vapi_subcount ON id=subcountid LEFT JOIN vapi_valcount ON id=valcountid
ORDER BY year, week, code;

CREATE OR REPLACE VIEW vlocaluser AS
SELECT *
FROM localuser;

CREATE OR REPLACE VIEW vsubscription AS
SELECT
ap.*,
lo.*,
su.subdate,
TO_CHAR(su.subdate, 'DD/MM') AS subdate_text,
su.validation,
su.validationdate
FROM subscribe su
RIGHT JOIN vapi ap ON ap.id=su.api
LEFT JOIN vlocaluser lo ON su.utclogin=lo.utclogin
ORDER BY ap.year, ap.week, ap.code, validation, subdate, lo.surname, lo.firstname, lo.utclogin;

CREATE OR REPLACE VIEW vsubscription_anonymous AS
SELECT normcode, name, normdbegin, normdend, size, level
FROM vsubscription;

CREATE OR REPLACE VIEW vstats AS
SELECT 	size.s AS size,
        total.c AS total, 
        subscription.c AS subscription, 
        waiting.c AS waiting, 
        total.c - subscription.c - waiting.c AS declined
FROM
(SELECT COUNT(DISTINCT CONCAT(utclogin, week)) as c FROM vsubscription WHERE utclogin IS NOT NULL) total
CROSS JOIN
(SELECT COUNT(DISTINCT CONCAT(utclogin, week)) as c FROM vsubscription WHERE validation) subscription
CROSS JOIN
(SELECT COUNT(DISTINCT CONCAT(utclogin, week)) as c FROM vsubscription WHERE validation IS NULL AND utclogin IS NOT NULL) waiting
CROSS JOIN 
(SELECT SUM(size) as s FROM api) size;

CREATE OR REPLACE VIEW vsubstats AS
SELECT TO_CHAR(subdate, 'WW') AS week, COUNT(utclogin) AS count 
FROM subscribe 
GROUP BY week 
ORDER BY week;

COMMIT;