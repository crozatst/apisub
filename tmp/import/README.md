# Procédures d'import

<!-- OBSOLETE TO BE DELETED AT NEXT INIT
## Import des niveaux des étudiants
fichier CSV : utclogin;level

```
DELETE FROM tmp;
DELETE FROM student;
CREATE tmp (a VARCHAR, b VARCHAR);
\copy tmp FROM '/home/apisub/students.csv' DELIMITER ';'
INSERT INTO student SELECT a, min(b) FROM tmp GROUP BY a;
```
-->

## Sync de la base de prod avec la base locale (pour tests)
su postgres
psql

```
\c postgres
DROP DATABASE apisub;
CREATE DATABASE apisub WITH OWNER apisub;
\c apisub
BEGIN;
\i /home/stc/Bureau/apisub20181213.sql
SELECT pg_catalog.set_config('search_path', 'public', false);
COMMIT;
```

## Manual INSERT of Api

INSERT INTO api (id,dbegin,code,name,description,duration,size,ects,resplogin) VALUES (
gen_random_uuid(),
TO_DATE('01/01/2020','DD/MM/YYYY'),
code,
'title',
'longtitle',
duration,
size,
ects,
'login'
);
