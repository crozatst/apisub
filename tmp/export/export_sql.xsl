<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:of="scpf.org:office"
    >

    <xsl:output method="text"></xsl:output>

    <xsl:variable name="year">
        <xsl:variable name="cattitle">
            <xsl:value-of select="/sc:item/of:webSite/of:webSiteM/sp:title"/>            
        </xsl:variable>
        <!-- warning this code is sensible to Scenari encoding of cat title & won't pass year 2100 -->
        <xsl:value-of select="concat('20', substring($cattitle, string-length($cattitle)-1, 2))"/>
    </xsl:variable>

    <xsl:variable name="logintitle">Login UTC du responsable UTC</xsl:variable>

    <xsl:template match="/sc:item">
        <xsl:apply-templates mode="login" select="of:webSite/sp:home/of:page/sp:subPage"/>
        <!-- Do not manage within transaction in order to add Apis step by step -->
        <!-- <xsl:text>BEGIN;</xsl:text> -->
        <xsl:apply-templates select="of:webSite/sp:home/of:page/sp:subPage"/>
        <!-- Manage case where an Api is repeted on two weeks (i.e. contains two lines with dates) : call again template, with param double-api on -->
        <xsl:apply-templates select="of:webSite/sp:home/of:page/sp:subPage[document(@sc:refUri)/sc:item//sp:info/of:block/of:blockM[sp:title='Période']/..//sc:para[2]]">
            <xsl:with-param name="double-api" select="1"/>
        </xsl:apply-templates>
        <!-- <xsl:text>COMMIT;</xsl:text> -->
    </xsl:template>

    <xsl:template match="sp:subPage" mode="login">
        <!-- Do not consider Api² (animer une Api) -->
        <xsl:if test="not(contains(@sc:refUri, 'animer-une-api'))">
            <xsl:text>INSERT INTO localuser (utclogin) VALUES ('</xsl:text>
            <xsl:value-of select="document(@sc:refUri)/sc:item//sp:info[of:block/of:blockM/sp:title=$logintitle]//sc:para[1]"/>
            <xsl:text>');</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="sp:subPage">
        <!-- Just propagate param double-api -->
        <xsl:param name="double-api"/>
        <!-- Do not consider Api² (animer une Api) -->
        <xsl:if test="not(contains(@sc:refUri, 'animer-une-api'))">
            <xsl:apply-templates select="document(@sc:refUri)/sc:item" mode="api">
                <xsl:with-param name="code">
                    <xsl:value-of select="substring(@sc:refUri,string-length(@sc:refUri)-11,4)"/>
                </xsl:with-param>
            <xsl:with-param name="double-api" select="$double-api"/>
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>

    <xsl:template match="sc:item" mode="api">
        <xsl:param name="code"/>
        <xsl:param name="double-api"/>
        <xsl:variable name="id">gen_random_uuid()</xsl:variable>
        <xsl:variable name="begin">
            <xsl:choose>
                <xsl:when test="$double-api">
                    <xsl:value-of select="substring(.//sp:info[of:block/of:blockM/sp:title='Période']//sc:para[2],1,5)"/>    
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring(.//sp:info[of:block/of:blockM/sp:title='Période']//sc:para[1],1,5)"/>    
                </xsl:otherwise>                
            </xsl:choose>                
        </xsl:variable>
        <xsl:variable name="end">
            <xsl:choose>
                <xsl:when test="$double-api">
                    <xsl:value-of select="substring(.//sp:info[of:block/of:blockM/sp:title='Période']//sc:para[2],7,5)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring(.//sp:info[of:block/of:blockM/sp:title='Période']//sc:para[1],7,5)"/>
                </xsl:otherwise>                
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="title">
            <xsl:value-of select="replace(of:section/of:sectionM/sp:title,'''','''''')"/>
        </xsl:variable>
        <xsl:variable name="longtitle">
            <xsl:value-of select="replace(.//sp:emphasis[1]//sc:para[1],'''','''''')"/>
        </xsl:variable>
        <xsl:variable name="duration">
            <xsl:variable name="firstdate">
                <xsl:variable name="day"><xsl:value-of select="substring($begin,1,2)"/></xsl:variable>
                <xsl:variable name="month"><xsl:value-of select="substring($begin,4,2)"/></xsl:variable>
                <xsl:value-of select="concat($year, '-', $month, '-', $day)"/>
            </xsl:variable>
            <xsl:variable name="lastdate">
                <xsl:variable name="day"><xsl:value-of select="substring($end,1,2)"/></xsl:variable>
                <xsl:variable name="month"><xsl:value-of select="substring($end,4,2)"/></xsl:variable>
                <xsl:value-of select="concat($year, '-', $month, '-', $day)"/>
            </xsl:variable>
            <xsl:value-of select="days-from-duration(xs:date($lastdate) - xs:date($firstdate)) + 1"/>             
        </xsl:variable>
        <xsl:variable name="size">
            <xsl:value-of select="number(.//sp:info[of:block/of:blockM/sp:title='Capacité d''accueil']//sc:para[1])"/>
        </xsl:variable>
        <xsl:variable name="ects">
            <xsl:value-of select="number(.//sp:info[of:block/of:blockM/sp:title='ECTS']//sc:para[1])"/>
        </xsl:variable>
        <xsl:variable name="login">
            <xsl:value-of select=".//sp:info[of:block/of:blockM/sp:title=$logintitle]//sc:para[1]"/>
        </xsl:variable>

INSERT INTO api (id,dbegin,code,name,description,duration,size,ects,resplogin) VALUES (
<xsl:value-of select="$id"/>,
TO_DATE('<xsl:value-of select="$begin"/>/<xsl:value-of select="$year"/>','DD/MM/YYYY'),
<xsl:value-of select="$code"/>,
'<xsl:value-of select="$title"></xsl:value-of>',
'<xsl:value-of select="$longtitle"></xsl:value-of>',
<xsl:value-of select="$duration"/>,
<xsl:value-of select="$size"/>,
<xsl:value-of select="$ects"/>,
'<xsl:value-of select="$login"/>'
<xsl:text>);</xsl:text>
    
    </xsl:template>



</xsl:stylesheet>
