#!/bin/bash
# use ssh key without password
# add ssh key to authorized_key
# testing access : ssh apisub@pic.crzt.fr -i /home/stph/.ssh/id_rsa_apisub

# add script ton local cron : 0 12 * * * /media/stph/data/git/apisub/sh/apisub_backup_rsync.sh >> /home/stph/cron/apisub.txt

# path to directory backup on server
user='apisub'
server='pic.crzt.fr'
serverpath='/home/apisub/backup/'

# path to directory backup on mirror
clientpath='/media/stph/data/backup/apisub/'

rsync -a --delete -e 'ssh -i ~/.ssh/id_rsa_apisub' $user@$server:$serverpath $clientpath
