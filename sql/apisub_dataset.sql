/** Test dataset **/
BEGIN;

DELETE FROM subscribe;
DELETE FROM api;
DELETE FROM localuser;

INSERT INTO localuser (utclogin) VALUES ('crozatst');
INSERT INTO localuser (utclogin) VALUES ('bob');
INSERT INTO localuser (utclogin) VALUES ('alice');
INSERT INTO localuser (utclogin) VALUES ('charlie');
INSERT INTO localuser (utclogin) VALUES ('dan');
INSERT INTO localuser (utclogin) VALUES ('eleonor');

INSERT INTO api ( id, dbegin, code, name, description, duration, size, ects, resplogin) VALUES (
  '1006e8dc-2178-11e9-8edb-4bc5e8f8599c',
  TO_DATE('20240110','yyyymmdd'),1,'Concevoir une formation','Concevoir et animer une formation courte',3,4,1,'bob'
);
INSERT INTO api ( id, dbegin, code, name, description, duration, size, ects, resplogin) VALUES (
  '5e06e8dc-2178-11e9-8edb-4bc5e8f8599c',
  TO_DATE('20240110','yyyymmdd'),5,'Poésie et ingénierie','Lorem ipsum dolor sit amet',5,3,2,'crozatst'
);
INSERT INTO api VALUES (
  '2afa8472-2178-11e9-b2b0-07511495b3b2',
  TO_DATE('20240110','yyyymmdd'),2,'Cloud big data blockchain IA','At vero eos et accusamus',3,12,1,'crozatst'
);
INSERT INTO api VALUES (
  '3352b3a0-217d-11e9-aabc-037b6e1a6a33',
  TO_DATE('20240117','yyyymmdd'),3,'La TRM et la Ğ1','Duis rhoncus turpis non libero auctor posuere.',5,10,2,'crozatst'
);
INSERT INTO api VALUES (
  '4352b3a0-217d-11e9-aabc-037b6e1a6a16',
  TO_DATE('20240117','yyyymmdd'),4,'Initiation au Picard','Vivamus porttitor arcu non dui mollis.',1,1,1,'crozatst'
);

/* Initial Subscriptions */

INSERT INTO subscribe (utclogin,api,subdate) VALUES (
  'bob','5e06e8dc-2178-11e9-8edb-4bc5e8f8599c',TO_DATE('20220121','yyyymmdd')
);
INSERT INTO subscribe (utclogin,api,subdate) VALUES (
  'bob','2afa8472-2178-11e9-b2b0-07511495b3b2',TO_DATE('20220121','yyyymmdd')
);
INSERT INTO subscribe (utclogin,api,subdate) VALUES (
  'bob','4352b3a0-217d-11e9-aabc-037b6e1a6a16',TO_DATE('20220121','yyyymmdd')
);
INSERT INTO subscribe (utclogin,api,subdate) VALUES (
  'alice','5e06e8dc-2178-11e9-8edb-4bc5e8f8599c',TO_DATE('20220201','yyyymmdd')
);
INSERT INTO subscribe (utclogin,api,subdate) VALUES (
  'charlie','5e06e8dc-2178-11e9-8edb-4bc5e8f8599c',TO_DATE('20220301','yyyymmdd')
);
INSERT INTO subscribe (utclogin,api,subdate) VALUES (
  'dan','2afa8472-2178-11e9-b2b0-07511495b3b2',TO_DATE('20220121','yyyymmdd')
);
INSERT INTO subscribe (utclogin,api,subdate) VALUES (
  'eleonor','4352b3a0-217d-11e9-aabc-037b6e1a6a16',TO_DATE('20220121','yyyymmdd')
);

SELECT normcode, name, size, nbval, nbask, open, resplogin, semester, year  FROM vapi;
SELECT * FROM vlocaluser;
SELECT utclogin, id, normcode, validation FROM vsubscription;

COMMIT;
