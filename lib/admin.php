<?php

class Admin {

  private $db;

  public function __construct ($db) {
    $this->db=$db;
  }

  public function isClosed() {
    return $this->db->config()['closed'];
  }

  public function isClosedDisplay() {
    if ($this->isClosed())
      return "Attention : Inscriptions actuellement désactivées par les responsables des Api";
    else
      return "(pas de désactivation manuelle)";
  }

  public function isActive() {
    return $this->db->config()['isactive'];
  }

  public function isActiveDisplay() {
    if ($this->isActive())
      return "Inscriptions ouvertes";
    else
      return "Inscriptions fermées";
  }

  public function isValidatable() {
    return $this->db->config()['isvalidatable'];
  }

  public function isValidatableDisplay() {
    if ($this->isValidatable())
      return "Validations ouvertes";
    else
      return "Validations fermées";
  }

  public function activeSemester() {
    return $this->db->config()['semester'];
  }

  public function activeYear() {
    return $this->db->config()['year'];
  }

  public function openBegin() {
    return $this->db->config()['openbegin'];
  }

  public function openEnd() {
    return $this->db->config()['openend'];
  }

  public function validationEnd() {
    return $this->db->config()['validationend'];
  }

  public function validationDays() {
    return $this->db->config()['validationdays'];
  }

}
