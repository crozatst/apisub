#!/bin/bash
# Execute as postgres user
# crontab -e : 0 * * * * /usr/bin/apisub_backup.sh
path='/home/apisub/backup'
date=`date -Iminutes`
echo "$date"
file="$path/apisub_$date.dump"
echo "Backuping apisub to $file"
pg_dump -cC --file $file apisub

# install stephane.crozat@utc.fr gpg key for postgres user with 
# gpg --keyserver keys.openpgp.org --search-keys stephane.crozat@utc.fr
gpg --yes --trust-model always --encrypt --recipient stephane.crozat@utc.fr $file
rm $file