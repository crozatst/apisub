#!/bin/bash
SH=`dirname $0`
SQL="$SH/../sql"
psql -c "DROP DATABASE IF EXISTS apisub"
psql -c "DROP USER IF EXISTS apisub"
psql -c "CREATE USER apisub WITH ENCRYPTED PASSWORD '$1'"
psql -c "CREATE DATABASE apisub WITH OWNER apisub"
psql -d apisub -f "$SQL/apisub_functions.sql"
psql -d apisub -f "$SQL/apisub_admin.sql"
psql -d apisub -f "$SQL/apisub_tables.sql"
psql -d apisub -f "$SQL/apisub_data.sql"
#psql -d apisub -c "\copy student (utclogin, level) FROM '$SQL/../data/students.csv' WITH CSV DELIMITER ';'" OBSOLETE TO BE DELETED
psql -d apisub -f "$SQL/apisub_views.sql"
psql -d apisub -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO apisub"
psql -d apisub -c 'CREATE EXTENSION IF NOT EXISTS "pgcrypto"'

# psql -d apisub -f "$SQL/apisub_dataset.sql"
