# Préambule

MAJ l'année dans le fichier export_sql.xsl : `<xsl:variable name="year">2023</xsl:variable>`

# Génération du fichier SQL à partir de l'archive XML de Scenari

Dans Scenari, depuis `catalogue.website`, exporter une archive `catalogue.scar`(clic droit) dans /tmp

```bash
cd /tmp
mv catalogue.scar catalogue.zip &&
rm -rf catxml &&
unzip catalogue.zip -d catxml
```

Ouvrir le fichier `catalogue.website` avec Oxygen ajouter le ligne suivante juste avant la balise racine `sc:item` :

```xml
<?xml-stylesheet href="/media/stc/data/git/apisub/tmp/export/export_sql.xsl" type="text/xsl"?>
```

Générer et récupérer le SQL :

```bash
cat /var/www/html/apisub/lib/connexion.php && psql -U apisub -d apisub -h localhost
```

NB : alternative à creuser : `xalan -in catxml/catalogue.website -out catalogue.sql -xsl /media/stc/data/git/apisub/tmp/export/export_sql.xsl`

# MAJ site sur le serveur web

Générer et télécharger depuis Scenari

```bash
rm -rf apihtml &&
unzip api_gen_simpleSite.zip -d apihtml &&
rsync -va apihtml/* wapint@stargate.utc.fr:/volgen/user1x/users/wapint/public_html/
```

# MAJ catalogue sur le serveur web

```bash
rm -rf cathtml &&
unzip catalogue_gen_site.zip -d cathtml &&
rsync -va cathtml/* wapint@stargate.utc.fr:/volgen/user1x/users/wapint/public_html/cat/
```

# Backup

```bash
su postgres
d=$(date +"%Y%m%d-%H%M%S");pg_dump apisub -f /home/apisub/backup/apisub$d.sql
scp stc@pic.crzt.fr:/home/apisub/backup/* .
```
