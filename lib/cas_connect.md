# Cas documentation

/**************************
* print_r($info) returns :
***************************
* Array
* (
*   [cas:user] => crozatst
*   [cas:attributes] => Array
*     (
*       [cas:uid] => crozatst
*       [cas:mail] => stephane.crozat@utc.fr
*       [cas:accountProfile] => utc-pers
*       [cas:laboratory] => COSTECH
*       [cas:displayName] => Stephane Crozat
*       [cas:ou] => staff
*       [cas:givenName] => Stephane
*       [cas:cn] => Stephane Crozat
*       [cas:sn] => Crozat
*       [cas:department] => GI
*     )
* )
***************************/

/***
 * 
 * frUTCStudentBranch: IM TC GI GB GSU GC ISC CH IDS HUTEC EPOGB EPOGA BIDM HIC ABIH MTSP EPOGC TCCPD
 * frUTCStudentSemester: 1..16
 * frUTCStudentSpecialization: IA CMI PI BAT ISIN MARS CIB AMT BB IDI 3ER MOPS BM MIT MPI ARS INES CTP AOS TBTS GPF BIOTE IAA AVI LIBR CP2R EPOGA SYM IL DMAR PV2R EPGB2 DCU EPGB1 TES EPGC3 IND FQI GSU ECOS SMC ISII B2R BMBI ...
 * 
 */
