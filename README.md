# Documentation

## Install

git@gitlab.utc.fr:crozatst/apisub.git

## Configuration

See INSTALL & MAJ

### `TABLE config`

- Set `closed` to `TRUE` to disable subscription (and unsubscription)
- Set `closed` to `FALSE` to enable subscription (and unsubscription) and set openbegin and openend to inscription dates.

### Close website

```bash
cd /var/www/html/apisub
mv index.php index_opened.php
cp closed/index.html .
```

### Reopen website

```bash
cd /var/www/html/apisub
rm index.html
mv index_opened.php index.php
```
