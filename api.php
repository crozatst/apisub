<?php
if (!isset($_GET['api'])) {
  header('Location:index.php');
}
else {
  session_start();
  include 'lib/cas_connect.php';
  include 'lib/db.php';
  include 'lib/views.php';
  include 'lib/admin.php';

  $db = new DB();
  $admin = new Admin($db);
  $view = new Views($admin, $_SESSION['utclogin'], $_SESSION['surname'], $_SESSION['firstname']);

  $view->printHtmlBegin($admin);
  $view->printUser($db->isResp($_SESSION['utclogin']), $db->isAdmin($_SESSION['utclogin']));
  $view->apiStudents($db->apiStudents($_SESSION['utclogin'],$_GET['api']));
  $view->apiStudents($db->apiWaitingStudents($_SESSION['utclogin'],$_GET['api']), false);
}

?>
