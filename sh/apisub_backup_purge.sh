#!/bin/bash
# Execute as postgres user on server
# crontab -e : 30 0 * * * /usr/bin/apisub_backup.sh
path='/home/apisub/backup'
mkdir "$path/keep"
# keep 96 last files (4 days)
for f in `find $path -maxdepth 1 -type f -name "*.dump" | sort -nr | head -n 96`; do mv $f "$path/keep"; done
# delete files older than 1 year
for f in `find $path -maxdepth 1 -type f -name "*.dump" -mtime +365`; do rm -f $f; done
# keep 1 file for each day 
for f in `find $path -maxdepth 1 -type f -name "*T00:*.dump"`; do mv $f "$path/keep"; done
# rm all files not kept
rm -f $path/*.dump
mv $path/keep/* .
rm -rf $path/keep