CREATE OR REPLACE VIEW analyse_subandval AS
SELECT *
FROM 
(SELECT subdate, COUNT(*) AS subscription FROM subscribe GROUP BY subdate) s
FULL OUTER JOIN
(SELECT validationdate, COUNT(*) AS validation FROM subscribe GROUP BY validationdate) v
ON validationdate=subdate
ORDER BY subdate, validationdate;

CREATE OR REPLACE VIEW analyse_nbcosub AS
SELECT c AS cosub, COUNT(*) AS nb 
FROM 
(
SELECT a.dbegin AS d, COUNT(*) AS c
FROM subscribe s JOIN api a
ON s.api=a.id
GROUP BY s.utclogin, a.dbegin
) s
GROUP BY s.c
ORDER BY s.c;
