<?php
include "xmltoarray.php";

class CAS {

	private $server;
	private $protocol;

	public function __construct ($server='https://cas.utc.fr/cas/', $protocol='https') {
		$this->server = $server;
		$this->protocol = $protocol;
	}

	public function connect() {
		if (!isset($_SESSION['ticket'])) {
			/*  If session is already open for current PHP file, user is already logged in, do nothing
				If session is not open on current PHP file, check if it is open on CAS server
					If it is open on CAS server retrieve session informations
					If it is not on open on CAS server, ask for login to CAS server
			*/
			$info = $this->authenticate();
			
			if ($info != -1) {
				/** TEST RETOURS CAS (TO DELETE) */
				//echo '<span style="font-size:0">';
				//print_r($info['cas:attributes']);
				//echo '</span>';
				/** FIN TEST RETOURS CAS */
				$_SESSION['ticket'] = $_GET['ticket'];
				$_SESSION['utclogin'] = $info['cas:user'];
				$_SESSION['mail'] = $info['cas:attributes']['cas:mail'];
				$_SESSION['surname'] = strtoupper($info['cas:attributes']['cas:sn']);
				$_SESSION['firstname'] = $info['cas:attributes']['cas:givenName'];
				if (isset($info['cas:attributes']['cas:studentBranch']))
					if (isset($info['cas:attributes']['cas:studentSpecialization']))						
						$_SESSION['level'] = $this->formatLevel(
							$info['cas:attributes']['cas:studentBranch'],
							$info['cas:attributes']['cas:studentSemester'],
							$info['cas:attributes']['cas:studentSpecialization']
						);
					else 
						$_SESSION['level'] = $this->formatLevel(
							$info['cas:attributes']['cas:studentBranch'],
							$info['cas:attributes']['cas:studentSemester']							
						);			
				else
					$_SESSION['level'] = 'unknown';				
			}
			else {
				$this->login();
			}
			$_SESSION['dolocalcopy'] = true; //call localcopy once per session only
		}
	}

	private function formatLevel ($branch, $semestre, $specialization = false) {
		/** FIX CAS not up to date */
		if ($branch == 'GSU')
			$branch = 'GU';
		elseif ($branch == 'GC')
			$branch = 'GP';
		elseif ($branch == 'HUTEC')
			$branch = 'HU';
		/** Separate branch from specialization */
		if ($specialization)
			$specialization = '/' . $specialization;
		/** Add zero before level */
		return $branch . '0' . $semestre . $specialization;
	}

	public function authenticate()	{
		if (!isset($_GET['ticket']) || empty($_GET['ticket']))
			return -1;

		$data = file_get_contents($this->server.'serviceValidate?service='.$this->protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    //'&ticket='.$_GET['ticket'] inutile?
		if (empty($data))
			return -1;

		$parsed = new xmlToArrayParser($data);

		if (!isset($parsed->array['cas:serviceResponse']['cas:authenticationSuccess']))
			return -1;

		return $parsed->array['cas:serviceResponse']['cas:authenticationSuccess'];
	}

	public function login() {
		header('Location: '.$this->server.'login?service='.$this->protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	}

	public function logout() {
		echo "<p>Yo</p>";
		session_destroy();
		header('Location: '.$this->server.'logout?service='.$this->protocol.'://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']));
	}

}
